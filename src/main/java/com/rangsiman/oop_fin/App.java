/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rangsiman.oop_fin;

import java.io.Serializable;

/**
 *
 * @author ADMIN
 */
public class App implements Serializable{
    private String name;
    private String flavor;
    private String size;
    private String drink;
    private String address;

    public App(String name, String flavor, String size, String drink, String address) {
        this.name = name;
        this.flavor = flavor;
        this.size = size;
        this.drink = drink;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlavor() {
        return flavor;
    }

    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }
    
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Order{" + "name=" + name + ", flavor=" + flavor + ", size=" + size + ", drink=" + drink +  ", address=" + address + '}';
    }
    
}

